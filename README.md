# Desafio Poli - ZSSN API

### Pré-requisitos
- Docker instalado e rodando
- Todos os contâineres rodando na máquina deverão estar parados
- Se houver um servidor apache rodando, deverá ser parado, a fim de não conflitar com o docker

### Instalação:
```bash
# Clonar o projeto deste repositório
$ git clone https://github.com/sseduardo13/poli.git

# Acessar a pasta do projeto
$ cd poli

# Instalar o projeto
$ docker run --rm --interactive --tty --volume $PWD:/app composer install

# Renomear o arquivo .env.example
$ mv .env.example .env

# Fazer o build
$ ./vendor/bin/sail build

# Executar o docker (sail)
$ ./vendor/bin/sail up
```

Em outra aba do terminal executar:

```bash
# Gerar a chave encriptada para o projeto
$ ./vendor/bin/sail artisan key:generate

# Aplicar configurações do arquivo .env
$ ./vendor/bin/sail artisan config:cache

# Criar tabelas no banco de dados
$ ./vendor/bin/sail artisan migrate

# Popular tabelas no banco de dados
$ ./vendor/bin/sail artisan db:seed
```

Manter aberto o terminal durante a execução, para finalizar a execução utilize as teclas `Ctrl + C`.

### Execução:
A cada nova execução deverá ser utilizado o comando abaixo dentro da pasta do projeto

```bash
 $ ./vendor/bin/sail up
```

Manter aberto o terminal durante a execução, para finalizar a execução utilize as teclas `Ctrl + C`.

### Testes:
Para executar os testes deverá ser utilizado o comando:

```bash
 $ ./vendor/bin/phpunit
```

### Acesso
Para utlização das rotas da API deverá ser utilizado um client como Postman por exemplo.
Para o uso no Postman poderá importar o json collection disponível em https://www.getpostman.com/collections/e02175fa4a029414e73d

### Tecnologias utilizadas
- Laravel: 9
- PHP: 8
- MySql: 8
