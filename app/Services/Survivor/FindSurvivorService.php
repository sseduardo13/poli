<?php

namespace App\Services\Survivor;

use App\Repositories\Survivor\Contracts\FindSurvivorByIdRepository;
use App\Services\Survivor\Contracts\FindSurvivorServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindSurvivorService implements FindSurvivorServiceContract
{
    /**
     * @var FindSurvivorByIdRepository
     */
    protected FindSurvivorByIdRepository $findSurvivorByIdRepository;

    /**
     * @param FindSurvivorByIdRepository $findSurvivorByIdRepository
     */
    public function __construct(FindSurvivorByIdRepository $findSurvivorByIdRepository)
    {
        $this->findSurvivorByIdRepository = $findSurvivorByIdRepository;
    }

    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function find(int $survivorId): Model|Exception|null
    {
        try {
            return $this->findSurvivorByIdRepository->find($survivorId);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}