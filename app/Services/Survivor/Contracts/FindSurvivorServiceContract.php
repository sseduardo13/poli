<?php

namespace App\Services\Survivor\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Model;

interface FindSurvivorServiceContract
{
    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function find(int $survivorId): Model|Exception|null;
}