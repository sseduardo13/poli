<?php

namespace App\Services\Survivor\Contracts;

use Exception;

interface SurvivorServiceContract
{
    /**
     * @param array $attributes
     * @return array|Exception|null
     * @throws Exception
     */
    public function addSurvivor(array $attributes): array|Exception|null;
}