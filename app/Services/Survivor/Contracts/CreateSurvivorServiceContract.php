<?php

namespace App\Services\Survivor\Contracts;

use App\Models\Survivor;
use Exception;

interface CreateSurvivorServiceContract
{
    /**
     * @param array $attributes
     * @return Survivor|Exception
     * @throws Exception
     */
    public function create(array $attributes): Survivor|Exception;
}