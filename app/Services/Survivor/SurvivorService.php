<?php

namespace App\Services\Survivor;

use App\Models\Survivor;
use App\Services\Infected\Contracts\CreateInfectedServiceContract;
use App\Services\Inventory\Contracts\CreateInventoryServiceContract;
use App\Services\Item\Contracts\FindItemServiceContract;
use App\Services\Location\Contracts\CreateLocationServiceContract;
use App\Services\Survivor\Contracts\CreateSurvivorServiceContract;
use App\Services\Survivor\Contracts\SurvivorServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SurvivorService implements SurvivorServiceContract
{
    /**
     * @var CreateInfectedServiceContract
     */
    protected CreateInfectedServiceContract $createInfectedService;

    /**
     * @var CreateInventoryServiceContract
     */
    protected CreateInventoryServiceContract $createInventoryService;
    
    /**
     * @var CreateLocationServiceContract
     */
    protected CreateLocationServiceContract $createLocationService;
    
    /**
     * @var CreateSurvivorServiceContract
     */
    protected CreateSurvivorServiceContract $createSurvivorService;

    /**
     * @var FindItemServiceContract
     */
    protected FindItemServiceContract $findItemService;
    
    /**
     * @param CreateInfectedServiceContract $createInfectedService
     * @param CreateInventoryServiceContract $createInventoryService
     * @param CreateLocationServiceContract $createLocationService
     * @param CreateSurvivorServiceContract $createSurvivorService
     * @param FindItemServiceContract $findItemService
     */
    public function __construct(
        CreateInfectedServiceContract $createInfectedService,
        CreateInventoryServiceContract $createInventoryService,
        CreateLocationServiceContract $createLocationService,
        CreateSurvivorServiceContract $createSurvivorService,
        FindItemServiceContract $findItemService
    )
    {
        $this->createInfectedService = $createInfectedService;
        $this->createInventoryService = $createInventoryService;
        $this->createLocationService = $createLocationService;
        $this->createSurvivorService = $createSurvivorService;
        $this->findItemService = $findItemService;
    }

    public function addSurvivor(array $attributes): array|Exception|null
    {
        try {
            DB::beginTransaction();

            $survivor = $this->createSurvivorService->create($attributes);
            $location = $this->createLocationService->create([
                'survivor_id' => $survivor->id,
                'latitude' => $attributes['last_location']['latitude'],
                'longitude' => $attributes['last_location']['longitude']
            ]);
            
            if (isset($attributes['items']) && ! empty($attributes['items'])) {
                foreach ($attributes['items'] as $item) {
                    $item = $this->findItemService->findByName(strtolower($item['name']));

                    if (! $item) {
                        throw new Exception("Item not found", 404);
                    }

                    $this->createInventoryService->create([
                        'survivor_id' => $survivor->id,
                        'item_id' => $item->id
                    ]);
                }
            }

            $this->createInfectedService->create([
                'survivor_id' => $survivor->id
            ]);

            DB::commit();
            
            $response = [];

            $response['survivor'] = $survivor;
            $response['location'] = $location;
            $response['inventory'] = $survivor->inventory()->get()->toArray();

            return $response;
        } catch (Exception $exception) {
            DB::rollBack();
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}