<?php

namespace App\Services\Survivor;

use App\Models\Survivor;
use App\Repositories\Survivor\Contracts\CreateSurvivorRepository;
use App\Services\Survivor\Contracts\CreateSurvivorServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateSurvivorService implements CreateSurvivorServiceContract
{
    /**
     * @var CreateSurvivorRepository
     */
    protected CreateSurvivorRepository $createSurvivorRepository;

    /**
     * @param CreateSurvivorRepository $createSurvivorRepository
     */
    public function __construct(CreateSurvivorRepository $createSurvivorRepository)
    {
        $this->createSurvivorRepository = $createSurvivorRepository;
    }

    /**
     * @param array $attributes
     * @return Survivor|Exception
     * @throws Exception
     */
    public function create(array $attributes): Survivor|Exception
    {
        try {
            return $this->createSurvivorRepository->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}