<?php

namespace App\Services\Report\Contracts;

use Exception;

interface InfectedSurvivorsServiceContract
{
    /**
     * @return float|Exception
     * @throws Exception
     */
    public function findAll(): float|Exception;
}