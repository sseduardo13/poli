<?php

namespace App\Services\Report\Contracts;

use Exception;

interface NonInfectedSurvivorsServiceContract
{
    /**
     * @return float|Exception
     * @throws Exception
     */
    public function findAll(): float|Exception;
}