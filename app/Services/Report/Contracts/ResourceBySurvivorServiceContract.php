<?php

namespace App\Services\Report\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Collection;

interface ResourceBySurvivorServiceContract
{
    /**
     * @return Collection|Exception
     * @throws Exception
     */
    public function find(): Collection|Exception;
}