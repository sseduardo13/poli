<?php

namespace App\Services\Report;

use App\Repositories\Report\Contracts\ResourceBySurvivorRepository;
use App\Services\Report\Contracts\ResourceBySurvivorServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class ResourceBySurvivorService implements ResourceBySurvivorServiceContract
{
    /**
     * @var ResourceBySurvivorRepository
     */
    protected ResourceBySurvivorRepository $resourceBySurvivorRepository;

    /**
     * @param ResourceBySurvivorRepository $resourceBySurvivorRepository
     */
    public function __construct(ResourceBySurvivorRepository $resourceBySurvivorRepository)
    {
        $this->resourceBySurvivorRepository = $resourceBySurvivorRepository;
    }

    /**
     * @return Collection|Exception
     * @throws Exception
     */
    public function find(): Collection|Exception
    {
        try {
            return $this->resourceBySurvivorRepository->find();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}