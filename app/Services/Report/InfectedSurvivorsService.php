<?php

namespace App\Services\Report;

use App\Repositories\Report\Contracts\InfectedSurvivorsRepository;
use App\Services\Report\Contracts\InfectedSurvivorsServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class InfectedSurvivorsService implements InfectedSurvivorsServiceContract
{
    /**
     * @var InfectedSurvivorsRepository
     */
    protected InfectedSurvivorsRepository $infectedSurvivorsRepository;

    /**
     * @param InfectedSurvivorsRepository $infectedSurvivorsRepository
     */
    public function __construct(InfectedSurvivorsRepository $infectedSurvivorsRepository)
    {
        $this->infectedSurvivorsRepository = $infectedSurvivorsRepository;
    }

    /**
     * @return float|Exception
     * @throws Exception
     */
    public function findAll(): float|Exception
    {
        try {
            $infectedsRate = $this->infectedSurvivorsRepository->findAll();
            return $infectedsRate * 100;
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}