<?php

namespace App\Services\Report;

use App\Repositories\Report\Contracts\PointsLostRepository;
use App\Services\Report\Contracts\PointsLostServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class PointsLostService implements PointsLostServiceContract
{
    /**
     * @var PointsLostRepository
     */
    protected PointsLostRepository $pointsLostRepository;

    /**
     * @param PointsLostRepository $pointsLostRepository
     */
    public function __construct(PointsLostRepository $pointsLostRepository)
    {
        $this->pointsLostRepository = $pointsLostRepository;
    }

    /**
     * @return Collection|Exception
     * @throws Exception
     */
    public function find(): Collection|Exception
    {
        try {
            return $this->pointsLostRepository->find();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}