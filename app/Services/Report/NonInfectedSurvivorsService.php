<?php

namespace App\Services\Report;

use App\Repositories\Report\Contracts\NonInfectedSurvivorsRepository;
use App\Services\Report\Contracts\NonInfectedSurvivorsServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class NonInfectedSurvivorsService implements NonInfectedSurvivorsServiceContract
{
    /**
     * @var NonInfectedSurvivorsRepository
     */
    protected NonInfectedSurvivorsRepository $nonInfectedSurvivorsRepository;

    /**
     * @param NonInfectedSurvivorsRepository $nonInfectedSurvivorsRepository
     */
    public function __construct(NonInfectedSurvivorsRepository $nonInfectedSurvivorsRepository)
    {
        $this->nonInfectedSurvivorsRepository = $nonInfectedSurvivorsRepository;
    }

    /**
     * @return float|Exception
     * @throws Exception
     */
    public function findAll(): float|Exception
    {
        try {
            $infectedsRate = $this->nonInfectedSurvivorsRepository->findAll();
            return $infectedsRate * 100;
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}