<?php

namespace App\Services\Trade;

use App\Services\Inventory\Contracts\CreateInventoryServiceContract;
use App\Services\Inventory\Contracts\FindInventoryServiceContract;
use App\Services\Inventory\Contracts\UpdateInventoryServiceContract;
use App\Services\Item\Contracts\FindItemServiceContract;
use App\Services\Survivor\Contracts\FindSurvivorServiceContract;
use App\Services\Trade\Contracts\TradeServiceContract;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TradeService implements TradeServiceContract
{
    /**
     * @var CreateInventoryServiceContract
     */
    protected CreateInventoryServiceContract $createInventoryService;

    /**
     * @var FindInventoryServiceContract
     */
    protected FindInventoryServiceContract $findInventoryService;
    
    /**
     * @var FindItemServiceContract
     */
    protected FindItemServiceContract $findItemService;

    /**
     * @var FindSurvivorServiceContract
     */
    protected FindSurvivorServiceContract $findSurvivorService;
    
    /**
     * @var UpdateInventoryServiceContract
     */
    protected UpdateInventoryServiceContract $updateInventoryService;
  
    /**
     * @param CreateInventoryServiceContract $createInventoryService
     * @param FindInventoryServiceContract $findInventoryService
     * @param FindItemServiceContract $findItemService
     * @param FindSurvivorServiceContract $findSurvivorService
     * @param UpdateInventoryServiceContract $updateInventoryService
     */
    public function __construct(
        CreateInventoryServiceContract $createInventoryService,
        FindInventoryServiceContract $findInventoryService,
        FindItemServiceContract $findItemService,
        FindSurvivorServiceContract $findSurvivorService,
        UpdateInventoryServiceContract $updateInventoryService
    )
    {
        $this->createInventoryService = $createInventoryService;
        $this->findInventoryService = $findInventoryService;
        $this->findItemService = $findItemService;
        $this->findSurvivorService = $findSurvivorService;
        $this->updateInventoryService = $updateInventoryService;
    }

    /**
     * @param array $transactions
     * @return Exception|array|null
     * @throws Exception
     */
    public function makeTransaction(array $transactions): Exception|array|null
    {
        try {
            DB::beginTransaction();

            $trade = [];
            foreach($transactions as $transaction) {
                $sender = $this->findSurvivorService->find($transaction['sender_id']);
                $this->verifyInfection($sender);
                $senderInventory = $this->getInventory($sender);

                $receiver = $this->findSurvivorService->find($transaction['receiver_id']);
                $this->verifyInfection($receiver);
                $receiverInventory = $this->getInventory($receiver);

                $points = 0;
                foreach ($transaction['items'] as $item) {
                    $itemForTrade = $this->verifyItem($item);
                    $points += $item['quantity'] * $itemForTrade['points'];

                    foreach ($senderInventory as $inventory) {
                        $this->verifyItemInInventory($item['item_id'], $sender->id);

                        if ($inventory['item_id'] == $item['item_id']) {
                            $quantity = $inventory['quantity'] - $item['quantity'];
                            $this->updateInventoryService->update(['quantity' => $quantity], $inventory['id']);
                        }
                    }
                    
                    foreach ($receiverInventory as $inventory) {
                        $inventoryItem = $this->findInventoryService->findByItemIdAndSurvivorId($item['item_id'], $receiver->id);

                        if (! $inventoryItem) {
                            $this->createInventoryService->create([
                                'survivor_id' => $receiver->id,
                                'item_id' => $item['item_id'],
                                'quantity' => $item['quantity']
                            ]);
                            continue;
                        }

                        if ($inventory['item_id'] == $item['item_id']) {
                            $quantity = $inventory['quantity'] + $item['quantity'];
                            $this->updateInventoryService->update(['quantity' => $quantity], $inventory['id']);
                        }
                    }
                }
                array_push($trade, $points);
            }

            if ($this->verifyTrade($trade)) {
                DB::commit();
            }

            return [
                'survivor1' => $this->getInventory($receiver),
                'survivor2' => $this->getInventory($sender)
            ];
        } catch (Exception $exception) {
            DB::rollBack();
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }

    private function verifyInfection($survivor)
    {
        $infected = $survivor->infected()->pluck('infected')->first();

        if ($infected) {
            throw new Exception("Survivor {$survivor->name} is infected. Transaction not allowed", 405);
        }
    }
    
    private function getInventory($survivor)
    {
        $inventory = $survivor->inventory()->first();

        if (! $inventory) {
            throw new Exception("Survivor {$survivor->name} not has items in your inventory. Transaction not allowed", 405);
        }

        return $survivor->inventory()->get(['id', 'item_id', 'quantity'])->toArray();
    }

    private function verifyItem($item)
    {
        $itemForTrade = $this->findItemService->find($item['item_id']);
    
        if (! $itemForTrade) {
            throw new Exception("Item not found. Transaction not allowed", 404);
        }

        return $itemForTrade;
    }

    private function verifyTrade($trade)
    {
        if ($trade[0] !== $trade[1]) {
            throw new Exception("The sum of points are not equals. Transaction not allowed", 405);
        }

        return true;
    }

    private function verifyItemInInventory($itemId, $survivorId)
    {
        $inventoryItem = $this->findInventoryService->findByItemIdAndSurvivorId($itemId, $survivorId);

        if (! $inventoryItem) {
            throw new Exception("The survivor has not item in your inventory. Transaction not allowed", 405);
        }
    }
}