<?php

namespace App\Services\Trade\Contracts;

use Exception;

interface TradeServiceContract
{
    /**
     * @param array $transactions
     * @return Exception|array|null
     * @throws Exception
     */
    public function makeTransaction(array $transactions): Exception|array|null;
}