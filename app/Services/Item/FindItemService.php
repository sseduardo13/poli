<?php

namespace App\Services\Item;

use App\Repositories\Item\Contracts\FindAllItemsRepository;
use App\Repositories\Item\Contracts\FindItemByIdRepository;
use App\Repositories\Item\Contracts\FindItemByNameRepository;
use App\Repositories\Item\FindAllItemsEloquentRepository;
use App\Services\Item\Contracts\FindItemServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Null_;

class FindItemService implements FindItemServiceContract
{
    /**
     * @var FindAllItemsRepository
     */
    protected FindAllItemsRepository $findAllItemsRepository;

    /**
     * @var FindItemByIdRepository
     */
    protected FindItemByIdRepository $findItemByIdRepository;
    
    /**
     * @var FindItemByNameRepository
     */
    protected FindItemByNameRepository $findItemByNameRepository;

    /**
     * @param FindAllItemsRepository $findAllItemsRepository
     * @param FindItemByIdRepository $findItemByIdRepository
     * @param FindItemByNameRepository $findItemByNameRepository
     */
    public function __construct(
        FindAllItemsRepository $findAllItemsRepository,
        FindItemByIdRepository $findItemByIdRepository,
        FindItemByNameRepository $findItemByNameRepository
    )
    {
        $this->findAllItemsRepository = $findAllItemsRepository;
        $this->findItemByIdRepository = $findItemByIdRepository;
        $this->findItemByNameRepository = $findItemByNameRepository;
    }

    /**
     * @param int $itemId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function find(int $itemId): Model|Exception|null
    {
        try {
            return $this->findItemByIdRepository->find($itemId);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
   
    /**
     * @param string $name
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findByName(string $name): Model|Exception|null
    {
        try {
            return $this->findItemByNameRepository->findByName($name);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }

    /**
    * @return Collection|Exception|null
    * @throws Exception
    */
    public function findAll(): Collection|Exception|null
    {
        try {
            return $this->findAllItemsRepository->findAll();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}