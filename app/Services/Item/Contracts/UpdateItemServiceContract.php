<?php

namespace App\Services\Item\Contracts;

use App\Models\Item;
use Exception;

interface UpdateItemServiceContract
{
    /**
     * @param array $attributes
     * @param $survivorId
     * @return Exception|Item
     * @throws Exception
     */
    public function update(array $attributes, $survivorId): Exception|Item;
}