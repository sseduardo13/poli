<?php

namespace App\Services\Item\Contracts;

use App\Models\Item;
use Exception;

interface DeleteItemServiceContract
{
    /**
     * @param $id
     * @return Exception|int
     * @throws Exception
     */
    public function delete(int $d): Exception|int|null;
}