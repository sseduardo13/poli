<?php

namespace App\Services\Item\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface FindItemServiceContract
{
    /**
     * @param int $itemId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function find(int $itemId): Model|Exception|null;

    /**
     * @param string $name
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findByName(string $name): Model|Exception|null;

    /**
    * @return Collection|Exception|null
    * @throws Exception
    */
    public function findAll(): Collection|Exception|null;
}