<?php

namespace App\Services\Item\Contracts;

use App\Models\Item;
use Exception;

interface CreateItemServiceContract
{
    /**
     * @param array $attributes
     * @return Item|Exception
     * @throws Exception
     */
    public function create(array $attributes): Item|Exception;
}