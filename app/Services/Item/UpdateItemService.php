<?php

namespace App\Services\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\UpdateItemRepository;
use App\Services\Item\Contracts\UpdateItemServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class UpdateItemService implements UpdateItemServiceContract
{
    /**
     * @var UpdateItemRepository
     */
    protected UpdateItemRepository $updateItemRepository;

    /**
     * @param UpdateItemRepository $updateItemRepository
     */
    public function __construct(UpdateItemRepository $updateItemRepository)
    {
        $this->updateItemRepository = $updateItemRepository;
    }

    /**
     * @param array $attributes
     * @param $id
     * @return Exception|Item
     * @throws Exception
     */
    public function update(array $attributes, $id): Exception|Item
    {
        try {
            return $this->updateItemRepository->update($attributes, $id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}