<?php

namespace App\Services\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\CreateItemRepository;
use App\Services\Item\Contracts\CreateItemServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateItemService implements CreateItemServiceContract
{
    /**
     * @var CreateItemRepository
     */
    protected CreateItemRepository $createItemRepository;

    /**
     * @param CreateItemRepository $createItemRepository
     */
    public function __construct(CreateItemRepository $createItemRepository)
    {
        $this->createItemRepository = $createItemRepository;
    }

    /**
     * @param array $attributes
     * @return Item|Exception
     * @throws Exception
     */
    public function create(array $attributes): Item|Exception
    {
        try {
            return $this->createItemRepository->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}