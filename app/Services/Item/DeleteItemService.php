<?php

namespace App\Services\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\DeleteItemRepository;
use App\Repositories\Item\Contracts\UpdateItemRepository;
use App\Services\Item\Contracts\DeleteItemServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class DeleteItemService implements DeleteItemServiceContract
{
    /**
     * @var DeleteItemRepository
     */
    protected DeleteItemRepository $deleteItemRepository;

    /**
     * @param DeleteItemRepository $deleteItemRepository
     */
    public function __construct(DeleteItemRepository $deleteItemRepository)
    {
        $this->deleteItemRepository = $deleteItemRepository;
    }

    /**
     * @param $id
     * @return Exception|int
     * @throws Exception
     */
    public function delete(int $id): Exception|int|null
    {
        try {
            return $this->deleteItemRepository->delete($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}