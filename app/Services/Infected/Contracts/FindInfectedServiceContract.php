<?php

namespace App\Services\Infected\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Model;

interface FindInfectedServiceContract
{
    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findBySurvivorId(int $survivorId): Model|Exception|null;
}