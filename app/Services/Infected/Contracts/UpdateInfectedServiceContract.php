<?php

namespace App\Services\Infected\Contracts;

use App\Models\Infected;
use Exception;

interface UpdateInfectedServiceContract
{
    /**
     * @param array $attributes
     * @param $survivorId
     * @return Exception|Infected
     * @throws Exception
     */
    public function update(array $attributes, $survivorId): Exception|Infected;
}