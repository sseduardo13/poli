<?php

namespace App\Services\Infected\Contracts;

use App\Models\Infected;
use Exception;

interface CreateInfectedServiceContract
{
    /**
     * @param array $attributes
     * @return Infected|Exception
     * @throws Exception
     */
    public function create(array $attributes): Infected|Exception;
}