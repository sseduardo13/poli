<?php

namespace App\Services\Infected;

use App\Models\Infected;
use App\Repositories\Infected\Contracts\CreateInfectedRepository;
use App\Services\Infected\Contracts\CreateInfectedServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateInfectedService implements CreateInfectedServiceContract
{
    /**
     * @var CreateInfectedRepository
     */
    protected CreateInfectedRepository $createInfectedRepository;

    /**
     * @param CreateInfectedRepository $createInfectedRepository
     */
    public function __construct(CreateInfectedRepository $createInfectedRepository)
    {
        $this->createInfectedRepository = $createInfectedRepository;
    }

    /**
     * @param array $attributes
     * @return Infected|Exception
     * @throws Exception
     */
    public function create(array $attributes): Infected|Exception
    {
        try {
            return $this->createInfectedRepository->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}