<?php

namespace App\Services\Infected;

use App\Models\Infected;
use App\Repositories\Infected\Contracts\FindInfectedBySurvivorIdRepository;
use App\Repositories\Infected\Contracts\UpdateInfectedRepository;
use App\Services\Infected\Contracts\UpdateInfectedServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class UpdateInfectedService implements UpdateInfectedServiceContract
{
    /**
     * @var FindInfectedBySurvivorIdRepository
     */
    protected FindInfectedBySurvivorIdRepository $findInfectedBySurvivorIdRepository;

    /**
     * @var UpdateInfectedRepository
     */
    protected UpdateInfectedRepository $updateInfectedRepository;

    /**
     * @param UpdateInfectedRepository $updateInfectedRepository
     */
    public function __construct(
        FindInfectedBySurvivorIdRepository $findInfectedBySurvivorIdRepository,
        UpdateInfectedRepository $updateInfectedRepository
    )
    {
        $this->findInfectedBySurvivorIdRepository = $findInfectedBySurvivorIdRepository;
        $this->updateInfectedRepository = $updateInfectedRepository;
    }

    /**
     * @param array $attributes
     * @param $survivorId
     * @return Exception|Infected
     * @throws Exception
     */
    public function update(array $attributes, $survivorId): Exception|Infected
    {
        try {
            $infected = $this->findInfectedBySurvivorIdRepository->findBySurvivorId($survivorId);

            if (! $infected) {
                throw new Exception("Survivor not found", 404);
            }

            $attributes['register'] = $infected->register + 1;
            $attributes['infected'] = $attributes['register'] >= 3 ? true : false;

            return $this->updateInfectedRepository->update($attributes, $infected->id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}