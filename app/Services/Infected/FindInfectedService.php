<?php

namespace App\Services\Infected;

use App\Repositories\Infected\Contracts\FindInfectedBySurvivorIdRepository;
use App\Services\Infected\Contracts\FindInfectedServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindInfectedService implements FindInfectedServiceContract
{
    /**
     * @var FindInfectedBySurvivorIdRepository
     */
    protected FindInfectedBySurvivorIdRepository $findInfectedBySurvivorIdRepository;

    /**
     * @param FindInfectedBySurvivorIdRepository $findInfectedBySurvivorIdRepository
     */
    public function __construct(FindInfectedBySurvivorIdRepository $findInfectedBySurvivorIdRepository)
    {
        $this->findInfectedBySurvivorIdRepository = $findInfectedBySurvivorIdRepository;
    }

    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findBySurvivorId(int $survivorId): Model|Exception|null
    {
        try {
            return $this->findInfectedBySurvivorIdRepository->findBySurvivorId($survivorId);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}