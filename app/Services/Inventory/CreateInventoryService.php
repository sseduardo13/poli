<?php

namespace App\Services\Inventory;

use App\Models\Inventory;
use App\Repositories\Inventory\Contracts\CreateInventoryRepository;
use App\Services\Inventory\Contracts\CreateInventoryServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateInventoryService implements CreateInventoryServiceContract
{
    /**
     * @var CreateInventoryRepository
     */
    protected CreateInventoryRepository $createInventoryRepository;

    /**
     * @param CreateInventoryRepository $createInventoryRepository
     */
    public function __construct(CreateInventoryRepository $createInventoryRepository)
    {
        $this->createInventoryRepository = $createInventoryRepository;
    }

    /**
     * @param array $attributes
     * @return Inventory|Exception
     * @throws Exception
     */
    public function create(array $attributes): Inventory|Exception
    {
        try {
            return $this->createInventoryRepository->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}