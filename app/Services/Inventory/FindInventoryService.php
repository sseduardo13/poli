<?php

namespace App\Services\Inventory;

use App\Repositories\Inventory\Contracts\FindInventoryByItemIdAndSurvivorIdRepository;
use App\Repositories\Inventory\Contracts\FindInventoryByItemIdRepository;
use App\Repositories\Inventory\Contracts\FindInventoryBySurvivorIdRepository;
use App\Services\Inventory\Contracts\FindInventoryServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindInventoryService implements FindInventoryServiceContract
{
    /**
     * @var FindInventoryBySurvivorIdRepository
     */
    protected FindInventoryBySurvivorIdRepository $findInventoryBySurvivorIdRepository;

    /**
     * @var FindInventoryByItemIdAndSurvivorIdRepository
     */
    protected FindInventoryByItemIdAndSurvivorIdRepository $findInventoryByItemIdAndSurvivorIdRepository;

    /**
     * @param FindInventoryBySurvivorIdRepository $findInventoryBySurvivorIdRepository
     * @param FindInventoryByItemIdAndSurvivorIdRepository $findInventoryByItemIdAndSurvivorIdRepository
     */
    public function __construct(
        FindInventoryBySurvivorIdRepository $findInventoryBySurvivorIdRepository,
        FindInventoryByItemIdAndSurvivorIdRepository $findInventoryByItemIdAndSurvivorIdRepository
    )
    {
        $this->findInventoryBySurvivorIdRepository = $findInventoryBySurvivorIdRepository;
        $this->findInventoryByItemIdAndSurvivorIdRepository = $findInventoryByItemIdAndSurvivorIdRepository;
    }

    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findBySurvivorId(int $survivorId): Model|Exception|null
    {
        try {
            return $this->findInventoryBySurvivorIdRepository->findBySurvivorId($survivorId);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
 
    /**
     * @param int $itemId
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findByItemIdAndSurvivorId(int $itemId, int $survivorId): Model|Exception|null
    {
        try {
            return $this->findInventoryByItemIdAndSurvivorIdRepository->findByItemIdAndSurvivorId($itemId, $survivorId);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}