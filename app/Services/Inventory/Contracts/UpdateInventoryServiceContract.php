<?php

namespace App\Services\Inventory\Contracts;

use App\Models\Inventory;
use Exception;

interface UpdateInventoryServiceContract
{
    /**
     * @param array $attributes
     * @param $id
     * @return Exception|Inventory
     * @throws Exception
     */
    public function update(array $attributes, $id): Exception|Inventory;
}