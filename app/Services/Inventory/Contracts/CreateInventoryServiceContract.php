<?php

namespace App\Services\Inventory\Contracts;

use App\Models\Inventory;
use Exception;

interface CreateInventoryServiceContract
{
    /**
     * @param array $attributes
     * @return Inventory|Exception
     * @throws Exception
     */
    public function create(array $attributes): Inventory|Exception;
}