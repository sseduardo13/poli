<?php

namespace App\Services\Inventory\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Model;

interface FindInventoryServiceContract
{
    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findBySurvivorId(int $survivorId): Model|Exception|null;
    
    /**
     * @param int $itemId
     * @param int $survivor
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findByItemIdAndSurvivorId(int $itemId, int $survivorId): Model|Exception|null;
}