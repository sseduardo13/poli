<?php

namespace App\Services\Inventory;

use App\Models\Inventory;
use App\Repositories\Inventory\Contracts\UpdateInventoryRepository;
use App\Services\Inventory\Contracts\UpdateInventoryServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class UpdateInventoryService implements UpdateInventoryServiceContract
{
    /**
     * @var UpdateInventoryRepository
     */
    protected UpdateInventoryRepository $updateInventoryRepository;

    /**
     * @param UpdateInventoryRepository $updateInventoryRepository
     */
    public function __construct(UpdateInventoryRepository $updateInventoryRepository)
    {
        $this->updateInventoryRepository = $updateInventoryRepository;
    }

    /**
     * @param array $attributes
     * @param $id
     * @return Exception|Inventory
     * @throws Exception
     */
    public function update(array $attributes, $id): Exception|Inventory
    {
        try {
            return $this->updateInventoryRepository->update($attributes, $id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}