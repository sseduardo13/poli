<?php

namespace App\Services\Location;

use App\Models\Location;
use App\Repositories\Location\Contracts\CreateLocationRepository;
use App\Services\Location\Contracts\CreateLocationServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateLocationService implements CreateLocationServiceContract
{
    /**
     * @var CreateLocationRepository
     */
    protected CreateLocationRepository $createLocationRepository;

    /**
     * @param CreateLocationRepository $createLocationRepository
     */
    public function __construct(CreateLocationRepository $createLocationRepository)
    {
        $this->createLocationRepository = $createLocationRepository;
    }

    /**
     * @param array $attributes
     * @return Location|Exception
     * @throws Exception
     */
    public function create(array $attributes): Location|Exception
    {
        try {
            return $this->createLocationRepository->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}