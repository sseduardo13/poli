<?php

namespace App\Services\Location;

use App\Models\Location;
use App\Repositories\Location\Contracts\FindLocationBySurvivorIdRepository;
use App\Repositories\Location\Contracts\UpdateLocationRepository;
use App\Services\Location\Contracts\UpdateLocationServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class UpdateLocationService implements UpdateLocationServiceContract
{
    /**
     * @var FindLocationBySurvivorIdRepository
     */
    protected FindLocationBySurvivorIdRepository $findLocationBySurvivorIdRepository;

    /**
     * @var UpdateLocationRepository
     */
    protected UpdateLocationRepository $updateLocationRepository;

    /**
     * @param UpdateLocationRepository $updateLocationRepository
     */
    public function __construct(
        FindLocationBySurvivorIdRepository $findLocationBySurvivorIdRepository,
        UpdateLocationRepository $updateLocationRepository
    )
    {
        $this->findLocationBySurvivorIdRepository = $findLocationBySurvivorIdRepository;
        $this->updateLocationRepository = $updateLocationRepository;
    }

    /**
     * @param array $attributes
     * @param $survivorId
     * @return Exception|Location
     * @throws Exception
     */
    public function update(array $attributes, $survivorId): Exception|Location
    {
        try {
            $location = $this->findLocationBySurvivorIdRepository->findBySurvivorId($survivorId);

            if (! $location) {
                throw new Exception("Survivor not found", 404);
            }

            return $this->updateLocationRepository->update($attributes, $location->id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}