<?php

namespace App\Services\Location\Contracts;

use App\Models\Location;
use Exception;

interface UpdateLocationServiceContract
{
    /**
     * @param array $attributes
     * @param $survivorId
     * @return Exception|Location
     * @throws Exception
     */
    public function update(array $attributes, $survivorId): Exception|Location;
}