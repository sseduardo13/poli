<?php

namespace App\Services\Location\Contracts;

use App\Models\Location;
use Exception;

interface CreateLocationServiceContract
{
    /**
     * @param array $attributes
     * @return Location|Exception
     * @throws Exception
     */
    public function create(array $attributes): Location|Exception;
}