<?php

namespace App\Services\Location\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Model;

interface FindLocationServiceContract
{
    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findBySurvivorId(int $survivorId): Model|Exception|null;
}