<?php

namespace App\Services\Location;

use App\Repositories\Location\Contracts\FindLocationBySurvivorIdRepository;
use App\Services\Location\Contracts\FindLocationServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindLocationService implements FindLocationServiceContract
{
    /**
     * @var FindLocationBySurvivorIdRepository
     */
    protected FindLocationBySurvivorIdRepository $findLocationBySurvivorIdRepository;

    /**
     * @param FindLocationBySurvivorIdRepository $findLocationBySurvivorIdRepository
     */
    public function __construct(FindLocationBySurvivorIdRepository $findLocationBySurvivorIdRepository)
    {
        $this->findLocationBySurvivorIdRepository = $findLocationBySurvivorIdRepository;
    }

    /**
     * @param int $survivorId
     * @return Model|Exception|null
     * @throws Exception
     */
    public function findBySurvivorId(int $survivorId): Model|Exception|null
    {
        try {
            return $this->findLocationBySurvivorIdRepository->findBySurvivorId($survivorId);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}