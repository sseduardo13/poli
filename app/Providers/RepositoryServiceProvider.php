<?php

namespace App\Providers;

use App\Repositories\Infected\Contracts\CreateInfectedRepository;
use App\Repositories\Infected\Contracts\FindInfectedBySurvivorIdRepository;
use App\Repositories\Infected\Contracts\UpdateInfectedRepository;
use App\Repositories\Infected\CreateInfectedEloquentRepository;
use App\Repositories\Infected\FindInfectedBySurvivorIdEloquentRepository;
use App\Repositories\Infected\UpdateInfectedEloquentRepository;
use App\Repositories\Inventory\Contracts\CreateInventoryRepository;
use App\Repositories\Inventory\Contracts\FindInventoryByItemIdAndSurvivorIdRepository;
use App\Repositories\Inventory\Contracts\FindInventoryBySurvivorIdRepository;
use App\Repositories\Inventory\Contracts\UpdateInventoryRepository;
use App\Repositories\Inventory\CreateInventoryEloquentRepository;
use App\Repositories\Inventory\FindInventoryByItemIdAndSurvivorIdEloquentRepository;
use App\Repositories\Inventory\FindInventoryBySurvivorIdEloquentRepository;
use App\Repositories\Inventory\UpdateInventoryEloquentRepository;
use App\Repositories\Item\Contracts\CreateItemRepository;
use App\Repositories\Item\Contracts\DeleteItemRepository;
use App\Repositories\Item\Contracts\FindAllItemsRepository;
use App\Repositories\Item\Contracts\FindItemByIdRepository;
use App\Repositories\Item\Contracts\FindItemByNameRepository;
use App\Repositories\Item\Contracts\UpdateItemRepository;
use App\Repositories\Item\CreateItemEloquentRepository;
use App\Repositories\Item\DeleteItemEloquentRepository;
use App\Repositories\Item\FindAllItemsEloquentRepository;
use App\Repositories\Item\FindItemByIdEloquentRepository;
use App\Repositories\Item\FindItemByNameEloquentRepository;
use App\Repositories\Item\UpdateItemEloquentRepository;
use App\Repositories\Location\Contracts\CreateLocationRepository;
use App\Repositories\Location\Contracts\FindLocationBySurvivorIdRepository;
use App\Repositories\Location\Contracts\UpdateLocationRepository;
use App\Repositories\Location\CreateLocationEloquentRepository;
use App\Repositories\Location\FindLocationBySurvivorIdEloquentRepository;
use App\Repositories\Location\UpdateLocationEloquentRepository;
use App\Repositories\Report\Contracts\InfectedSurvivorsRepository;
use App\Repositories\Report\Contracts\NonInfectedSurvivorsRepository;
use App\Repositories\Report\Contracts\PointsLostRepository;
use App\Repositories\Report\Contracts\ResourceBySurvivorRepository;
use App\Repositories\Report\InfectedSurvivorsEloquentRepository;
use App\Repositories\Report\NonInfectedSurvivorsEloquentRepository;
use App\Repositories\Report\PointsLostEloquentRepository;
use App\Repositories\Report\ResourceBySurvivorEloquentRepository;
use App\Repositories\Survivor\Contracts\CreateSurvivorRepository;
use App\Repositories\Survivor\Contracts\FindSurvivorByIdRepository;
use App\Repositories\Survivor\CreateSurvivorEloquentRepository;
use App\Repositories\Survivor\FindSurvivorByIdEloquentRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindInfected();
        $this->bindInventory();
        $this->bindItem();
        $this->bindLocation();
        $this->bindReport();
        $this->bindSurvivor();
    }

    public function bindInfected()
    {
        $this->app->bind(CreateInfectedRepository::class, CreateInfectedEloquentRepository::class);
        $this->app->bind(FindInfectedBySurvivorIdRepository::class, FindInfectedBySurvivorIdEloquentRepository::class);
        $this->app->bind(UpdateInfectedRepository::class, UpdateInfectedEloquentRepository::class);
    }

    public function bindInventory()
    {
        $this->app->bind(CreateInventoryRepository::class, CreateInventoryEloquentRepository::class);
        $this->app->bind(FindInventoryByItemIdAndSurvivorIdRepository::class, FindInventoryByItemIdAndSurvivorIdEloquentRepository::class);
        $this->app->bind(FindInventoryBySurvivorIdRepository::class, FindInventoryBySurvivorIdEloquentRepository::class);
        $this->app->bind(UpdateInventoryRepository::class, UpdateInventoryEloquentRepository::class);
    }

    public function bindItem()
    {
        $this->app->bind(CreateItemRepository::class, CreateItemEloquentRepository::class);
        $this->app->bind(DeleteItemRepository::class, DeleteItemEloquentRepository::class);
        $this->app->bind(FindAllItemsRepository::class, FindAllItemsEloquentRepository::class);
        $this->app->bind(FindItemByIdRepository::class, FindItemByIdEloquentRepository::class);
        $this->app->bind(FindItemByNameRepository::class, FindItemByNameEloquentRepository::class);
        $this->app->bind(UpdateItemRepository::class, UpdateItemEloquentRepository::class);
    }

    public function bindLocation()
    {
        $this->app->bind(CreateLocationRepository::class, CreateLocationEloquentRepository::class);
        $this->app->bind(FindLocationBySurvivorIdRepository::class, FindLocationBySurvivorIdEloquentRepository::class);
        $this->app->bind(UpdateLocationRepository::class, UpdateLocationEloquentRepository::class);
    }
    
    public function bindReport()
    {
        $this->app->bind(InfectedSurvivorsRepository::class, InfectedSurvivorsEloquentRepository::class);
        $this->app->bind(NonInfectedSurvivorsRepository::class, NonInfectedSurvivorsEloquentRepository::class);
        $this->app->bind(PointsLostRepository::class, PointsLostEloquentRepository::class);
        $this->app->bind(ResourceBySurvivorRepository::class, ResourceBySurvivorEloquentRepository::class);
    }

    public function bindSurvivor()
    {
        $this->app->bind(CreateSurvivorRepository::class, CreateSurvivorEloquentRepository::class);
        $this->app->bind(FindSurvivorByIdRepository::class, FindSurvivorByIdEloquentRepository::class);
    }
}