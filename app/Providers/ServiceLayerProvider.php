<?php

namespace App\Providers;

use App\Services\Infected\Contracts\CreateInfectedServiceContract;
use App\Services\Infected\Contracts\FindInfectedServiceContract;
use App\Services\Infected\Contracts\UpdateInfectedServiceContract;
use App\Services\Infected\CreateInfectedService;
use App\Services\Infected\FindInfectedService;
use App\Services\Infected\UpdateInfectedService;
use App\Services\Inventory\Contracts\CreateInventoryServiceContract;
use App\Services\Inventory\Contracts\FindInventoryServiceContract;
use App\Services\Inventory\Contracts\UpdateInventoryServiceContract;
use App\Services\Inventory\CreateInventoryService;
use App\Services\Inventory\FindInventoryService;
use App\Services\Inventory\UpdateInventoryService;
use App\Services\Item\Contracts\CreateItemServiceContract;
use App\Services\Item\Contracts\DeleteItemServiceContract;
use App\Services\Item\Contracts\FindItemServiceContract;
use App\Services\Item\Contracts\UpdateItemServiceContract;
use App\Services\Item\CreateItemService;
use App\Services\Item\DeleteItemService;
use App\Services\Item\FindItemService;
use App\Services\Item\UpdateItemService;
use App\Services\Location\Contracts\CreateLocationServiceContract;
use App\Services\Location\Contracts\FindLocationServiceContract;
use App\Services\Location\Contracts\UpdateLocationServiceContract;
use App\Services\Location\CreateLocationService;
use App\Services\Location\FindLocationService;
use App\Services\Location\UpdateLocationService;
use App\Services\Report\Contracts\InfectedSurvivorsServiceContract;
use App\Services\Report\Contracts\NonInfectedSurvivorsServiceContract;
use App\Services\Report\Contracts\PointsLostServiceContract;
use App\Services\Report\Contracts\ResourceBySurvivorServiceContract;
use App\Services\Report\InfectedSurvivorsService;
use App\Services\Report\NonInfectedSurvivorsService;
use App\Services\Report\PointsLostService;
use App\Services\Report\ResourceBySurvivorService;
use App\Services\Survivor\Contracts\CreateSurvivorServiceContract;
use App\Services\Survivor\Contracts\FindSurvivorServiceContract;
use App\Services\Survivor\Contracts\SurvivorServiceContract;
use App\Services\Survivor\CreateSurvivorService;
use App\Services\Survivor\FindSurvivorService;
use App\Services\Survivor\SurvivorService;
use App\Services\Trade\Contracts\TradeServiceContract;
use App\Services\Trade\TradeService;
use Illuminate\Support\ServiceProvider;

class ServiceLayerProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindInfected();
        $this->bindInventory();
        $this->bindItem();
        $this->bindLocation();
        $this->bindReport();
        $this->bindSurvivor();
        $this->bindTrade();
    }

    public function bindInfected()
    {
        $this->app->bind(CreateInfectedServiceContract::class, CreateInfectedService::class);
        $this->app->bind(FindInfectedServiceContract::class, FindInfectedService::class);
        $this->app->bind(UpdateInfectedServiceContract::class, UpdateInfectedService::class);
    }

    public function bindInventory()
    {
        $this->app->bind(CreateInventoryServiceContract::class, CreateInventoryService::class);
        $this->app->bind(FindInventoryServiceContract::class, FindInventoryService::class);
        $this->app->bind(UpdateInventoryServiceContract::class, UpdateInventoryService::class);
    }

    public function bindItem()
    {
        $this->app->bind(CreateItemServiceContract::class, CreateItemService::class);
        $this->app->bind(FindItemServiceContract::class, FindItemService::class);
        $this->app->bind(UpdateItemServiceContract::class, UpdateItemService::class);
        $this->app->bind(DeleteItemServiceContract::class, DeleteItemService::class);
    }

    public function bindLocation()
    {
        $this->app->bind(CreateLocationServiceContract::class, CreateLocationService::class);
        $this->app->bind(FindLocationServiceContract::class, FindLocationService::class);
        $this->app->bind(UpdateLocationServiceContract::class, UpdateLocationService::class);
    }

    public function bindReport()
    {
        $this->app->bind(InfectedSurvivorsServiceContract::class, InfectedSurvivorsService::class);
        $this->app->bind(NonInfectedSurvivorsServiceContract::class, NonInfectedSurvivorsService::class);
        $this->app->bind(PointsLostServiceContract::class, PointsLostService::class);
        $this->app->bind(ResourceBySurvivorServiceContract::class, ResourceBySurvivorService::class);
    }

    public function bindSurvivor()
    {
        $this->app->bind(CreateSurvivorServiceContract::class, CreateSurvivorService::class);
        $this->app->bind(FindSurvivorServiceContract::class, FindSurvivorService::class);
        $this->app->bind(SurvivorServiceContract::class, SurvivorService::class);
    }

    public function bindTrade()
    {
        $this->app->bind(TradeServiceContract::class, TradeService::class);
    }
}