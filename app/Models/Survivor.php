<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Survivor extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'age',
        'gender'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
    * @return HasOne
    */
    public function location(): HasOne
    {
        return $this->hasOne(Location::class);
    }
    
    /**
    * @return HasOne
    */
    public function infected(): HasOne
    {
        return $this->hasOne(Infected::class);
    }

    /**
    * @return HasOne
    */
    public function inventory(): HasOne
    {
        return $this->hasOne(Inventory::class);
    }

}
