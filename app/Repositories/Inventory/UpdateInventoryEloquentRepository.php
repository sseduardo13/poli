<?php

namespace App\Repositories\Inventory;

use App\Models\Inventory;
use App\Repositories\Inventory\Contracts\UpdateInventoryRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UpdateInventoryEloquentRepository implements UpdateInventoryRepository
{
    /**
     * @var Model|Inventory
     */
    private Model|Inventory $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Inventory();
    }

    /**
     * @param array $attributes
     * @param int $id
     * @return Inventory|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Inventory|Exception
    {
        try {

            $model = $this->eloquentModel->findOrFail($id);
            $model->fill($attributes);
            $model->save();

            return $model;

        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}