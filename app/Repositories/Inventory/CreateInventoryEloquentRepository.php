<?php

namespace App\Repositories\Inventory;

use App\Models\Inventory;
use App\Repositories\Inventory\Contracts\CreateInventoryRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CreateInventoryEloquentRepository implements CreateInventoryRepository
{
    /**
     * @var Model|Inventory
     */
    private Model|Inventory $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Inventory();
    }

    /**
     * @param array $attributes
     * @return Inventory|Exception
     * @throws Exception
     */
    public function create(array $attributes): Inventory|Exception
    {
        try {
            return $this->eloquentModel->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}