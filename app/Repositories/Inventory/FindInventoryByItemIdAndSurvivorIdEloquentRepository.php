<?php

namespace App\Repositories\Inventory;

use App\Models\Inventory;
use App\Repositories\Inventory\Contracts\FindInventoryByItemIdAndSurvivorIdRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindInventoryByItemIdAndSurvivorIdEloquentRepository implements FindInventoryByItemIdAndSurvivorIdRepository
{
    /**
     * @var Model|Inventory
     */
    private Model|Inventory $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Inventory();
    }

    /**
     * @param int $itemId
     * @param int $survivorId
     * @return Inventory|Exception|null
     */
    public function findByItemIdAndSurvivorId(int $itemId, int $survivorId): Inventory|Exception|null
    {
        try {
            return $this->eloquentModel
                ->where('item_id', $itemId)
                ->where('survivor_id', $survivorId)
                ->first();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}