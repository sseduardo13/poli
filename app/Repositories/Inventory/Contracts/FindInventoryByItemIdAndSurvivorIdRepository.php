<?php

namespace App\Repositories\Inventory\Contracts;

use App\Models\Inventory;
use Exception;

interface FindInventoryByItemIdAndSurvivorIdRepository
{
    /**
     * @param int $itemId
     * @param int $survivorId
     * @return Inventory|Exception|null
     */
    public function findByItemIdAndSurvivorId(int $itemId, int $survivorId): Inventory|Exception|null;
}