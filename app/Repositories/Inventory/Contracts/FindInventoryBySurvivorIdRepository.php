<?php

namespace App\Repositories\Inventory\Contracts;

use App\Models\Inventory;
use Exception;

interface FindInventoryBySurvivorIdRepository
{
    /**
     * @param int $survivorId
     * @return Inventory|Exception|null
     */
    public function findBySurvivorId(int $survivorId): Inventory|Exception|null;
}