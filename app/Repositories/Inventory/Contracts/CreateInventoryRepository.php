<?php

namespace App\Repositories\Inventory\Contracts;

use App\Models\Inventory;
use Exception;

interface CreateInventoryRepository
{    
    /**
     * @param array $attributes
     * @return Inventory|Exception
     * @throws Exception
     */
    public function create(array $attributes): Inventory|Exception;
}