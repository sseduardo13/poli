<?php

namespace App\Repositories\Inventory\Contracts;

use App\Models\Inventory;
use Exception;

interface UpdateInventoryRepository
{
    /**
     * @param array $attributes
     * @param int $id
     * @return Inventory|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Inventory|Exception;
}