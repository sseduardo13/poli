<?php

namespace App\Repositories\Inventory;

use App\Models\Inventory;
use App\Repositories\Inventory\Contracts\FindInventoryBySurvivorIdRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindInventoryBySurvivorIdEloquentRepository implements FindInventoryBySurvivorIdRepository
{
    /**
     * @var Model|Inventory
     */
    private Model|Inventory $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Inventory();
    }

    /**
     * @param int $survivorId
     * @return Inventory|Exception|null
     */
    public function findBySurvivorId(int $survivorId): Inventory|Exception|null
    {
        try {
            return $this->eloquentModel
                ->where('survivor_id', $survivorId)
                ->first();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}