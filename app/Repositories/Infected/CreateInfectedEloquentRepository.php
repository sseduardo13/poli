<?php

namespace App\Repositories\Infected;

use App\Models\Infected;
use App\Repositories\Infected\Contracts\CreateInfectedRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CreateInfectedEloquentRepository implements CreateInfectedRepository
{
    /**
     * @var Model|Infected
     */
    private Model|Infected $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Infected();
    }

    /**
     * @param array $attributes
     * @return Infected|Exception
     * @throws Exception
     */
    public function create(array $attributes): Infected|Exception
    {
        try {
            return $this->eloquentModel->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}