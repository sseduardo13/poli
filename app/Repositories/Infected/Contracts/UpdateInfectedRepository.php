<?php

namespace App\Repositories\Infected\Contracts;

use App\Models\Infected;
use Exception;

interface UpdateInfectedRepository
{
    /**
     * @param array $attributes
     * @param int $id
     * @return Infected|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Infected|Exception;
}