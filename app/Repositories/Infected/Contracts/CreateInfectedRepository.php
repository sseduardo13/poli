<?php

namespace App\Repositories\Infected\Contracts;

use App\Models\Infected;
use Exception;

interface CreateInfectedRepository
{    
    /**
     * @param array $attributes
     * @return Infected|Exception
     * @throws Exception
     */
    public function create(array $attributes): Infected|Exception;
}