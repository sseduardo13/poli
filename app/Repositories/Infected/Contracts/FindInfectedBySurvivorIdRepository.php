<?php

namespace App\Repositories\Infected\Contracts;

use App\Models\Infected;
use Exception;

interface FindInfectedBySurvivorIdRepository
{
    /**
     * @param int $survivorId
     * @return Infected|Exception|null
     */
    public function findBySurvivorId(int $survivorId): Infected|Exception|null;
}