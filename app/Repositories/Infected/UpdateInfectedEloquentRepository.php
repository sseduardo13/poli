<?php

namespace App\Repositories\Infected;

use App\Models\Infected;
use App\Repositories\Infected\Contracts\UpdateInfectedRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UpdateInfectedEloquentRepository implements UpdateInfectedRepository
{
    /**
     * @var Model|Infected
     */
    private Model|Infected $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Infected();
    }

    /**
     * @param array $attributes
     * @param int $id
     * @return Infected|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Infected|Exception
    {
        try {

            $model = $this->eloquentModel->findOrFail($id);
            $model->fill($attributes);
            $model->save();

            return $model;

        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}