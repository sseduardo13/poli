<?php

namespace App\Repositories\Infected;

use App\Models\Infected;
use App\Repositories\Infected\Contracts\FindInfectedBySurvivorIdRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindInfectedBySurvivorIdEloquentRepository implements FindInfectedBySurvivorIdRepository
{
    /**
     * @var Model|Infected
     */
    private Model|Infected $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Infected();
    }

    /**
     * @param int $survivorId
     * @return Infected|Exception|null
     */
    public function findBySurvivorId(int $survivorId): Infected|Exception|null
    {
        try {
            return $this->eloquentModel
                ->where('survivor_id', $survivorId)
                ->first();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}