<?php

namespace App\Repositories\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\DeleteItemRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class DeleteItemEloquentRepository implements DeleteItemRepository
{
    /**
     * @var Model|Item
     */
    private Model|Item $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Item();
    }

    /**
     * @param int $id
     * @return int|Exception|null
     * @throws Exception
     */
    public function delete(int $id): int|Exception|null
    {
        try {
            return $this->eloquentModel->destroy($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}