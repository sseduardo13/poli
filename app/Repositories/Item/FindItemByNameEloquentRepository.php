<?php

namespace App\Repositories\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\FindItemByNameRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindItemByNameEloquentRepository implements FindItemByNameRepository
{
    /**
     * @var Model|Item
     */
    private Model|Item $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Item();
    }

    /**
     * @param string $name
     * @return Item|Exception|null
     */
    public function findByName(string $name): Item|Exception|null
    {
        try {
            return $this->eloquentModel
                ->where('name', 'like', $name)
                ->first();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}