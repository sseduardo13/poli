<?php

namespace App\Repositories\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\UpdateItemRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UpdateItemEloquentRepository implements UpdateItemRepository
{
    /**
     * @var Model|Item
     */
    private Model|Item $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Item();
    }

    /**
     * @param array $attributes
     * @param int $id
     * @return Item|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Item|Exception
    {
        try {

            $model = $this->eloquentModel->findOrFail($id);
            $model->fill($attributes);
            $model->save();

            return $model;

        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}