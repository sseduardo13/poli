<?php

namespace App\Repositories\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\FindAllItemsRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindAllItemsEloquentRepository implements FindAllItemsRepository
{
    /**
     * @var Model|Item
     */
    private Model|Item $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Item();
    }

    /**
     * @return Item|Exception|null
     */
    public function findAll(): Collection|Exception|null
    {
        try {
            return $this->eloquentModel->all();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}