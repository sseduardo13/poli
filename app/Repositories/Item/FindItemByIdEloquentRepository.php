<?php

namespace App\Repositories\Item;

use App\Models\Item;
use App\Repositories\Item\Contracts\FindItemByIdRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindItemByIdEloquentRepository implements FindItemByIdRepository
{
    /**
     * @var Model|Item
     */
    private Model|Item $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Item();
    }

    /**
     * @param int $id
     * @return Item|Exception|null
     */
    public function find(int $id): Item|Exception|null
    {
        try {
            return $this->eloquentModel->find($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}