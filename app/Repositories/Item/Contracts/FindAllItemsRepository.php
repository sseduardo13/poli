<?php

namespace App\Repositories\Item\Contracts;

use App\Models\Item;
use Exception;
use Illuminate\Database\Eloquent\Collection;

interface FindAllItemsRepository
{
    /**
     * @return Collection|Exception|null
     */
    public function findAll(): Collection|Exception|null;
}