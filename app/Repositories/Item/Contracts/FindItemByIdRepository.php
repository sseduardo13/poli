<?php

namespace App\Repositories\Item\Contracts;

use App\Models\Item;
use Exception;

interface FindItemByIdRepository
{
    /**
     * @param int $id
     * @return Item|Exception|null
     */
    public function find(int $id): Item|Exception|null;
}