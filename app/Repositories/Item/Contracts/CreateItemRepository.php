<?php

namespace App\Repositories\Item\Contracts;

use App\Models\Item;
use Exception;

interface CreateItemRepository
{    
    /**
     * @param array $attributes
     * @return Item|Exception
     * @throws Exception
     */
    public function create(array $attributes): Item|Exception;
}