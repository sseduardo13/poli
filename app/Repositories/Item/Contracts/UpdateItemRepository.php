<?php

namespace App\Repositories\Item\Contracts;

use App\Models\Item;
use Exception;

interface UpdateItemRepository
{
    /**
     * @param array $attributes
     * @param int $id
     * @return Item|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Item|Exception;
}