<?php

namespace App\Repositories\Item\Contracts;

use App\Models\Item;
use Exception;

interface FindItemByNameRepository
{
    /**
     * @param string $name
     * @return Item|Exception|null
     */
    public function findByName(string $name): Item|Exception|null;
}