<?php

namespace App\Repositories\Item\Contracts;

use App\Models\Item;
use Exception;

interface DeleteItemRepository
{
    /**
     * @param int $id
     * @return int|Exception|null
     * @throws Exception
     */
    public function delete(int $id): int|Exception|null;
}