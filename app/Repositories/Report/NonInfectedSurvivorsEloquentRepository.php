<?php

namespace App\Repositories\Report;

use App\Models\Infected;
use App\Repositories\Report\Contracts\NonInfectedSurvivorsRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class NonInfectedSurvivorsEloquentRepository implements NonInfectedSurvivorsRepository
{
    /**
     * @var Model|Infected
     */
    private Model|Infected $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Infected();
    }

    /**
     * @return float|Exception|null
     */
    public function findAll(): float|Exception|null
    {
        try {
            return $this->eloquentModel
                ->all()
                ->average(function($value) {
                    return $value['infected'] == false;
                });
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}