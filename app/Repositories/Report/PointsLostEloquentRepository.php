<?php

namespace App\Repositories\Report;

use App\Models\Infected;
use App\Repositories\Report\Contracts\PointsLostRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class PointsLostEloquentRepository implements PointsLostRepository
{
    /**
     * @var Model|Infected
     */
    private Model|Infected $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Infected();
    }

    /**
     * @return Collection|Exception|null
     */
    public function find(): Collection|Exception|null
    {
        try {
            return $this->eloquentModel
                ->selectRaw("SUM(inventories.quantity * items.points) as total")
                ->join('inventories', 'inventories.survivor_id', '=', 'infecteds.survivor_id')
                ->join('items', 'items.id', '=', 'inventories.item_id')
                ->where('infected', 1)
                ->get();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}