<?php

namespace App\Repositories\Report\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Collection;

interface PointsLostRepository
{
    /**
     * @return Collection|Exception|null
     */
    public function find(): Collection|Exception|null;
}