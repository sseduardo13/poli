<?php

namespace App\Repositories\Report\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Collection;

interface ResourceBySurvivorRepository
{
    /**
     * @return Collection|Exception|null
     */
    public function find(): Collection|Exception|null;
}