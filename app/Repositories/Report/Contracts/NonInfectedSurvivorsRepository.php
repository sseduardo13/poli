<?php

namespace App\Repositories\Report\Contracts;

use Exception;

interface NonInfectedSurvivorsRepository
{
    /**
     * @return float|Exception|null
     */
    public function findAll(): float|Exception|null;
}