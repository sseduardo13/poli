<?php

namespace App\Repositories\Report\Contracts;

use Exception;

interface InfectedSurvivorsRepository
{
    /**
     * @return float|Exception|null
     */
    public function findAll(): float|Exception|null;
}