<?php

namespace App\Repositories\Report;

use App\Models\Infected;
use App\Repositories\Report\Contracts\ResourceBySurvivorRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ResourceBySurvivorEloquentRepository implements ResourceBySurvivorRepository
{
    /**
     * @var Model|Infected
     */
    private Model|Infected $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Infected();
    }

    /**
     * @return Collection|Exception|null
     */
    public function find(): Collection|Exception|null
    {
        try {
            return $this->eloquentModel
                ->select('items.name')
                ->selectRaw("SUM(inventories.quantity) / 
                    (
                        SELECT
                            COUNT(infecteds.id)
                        FROM
                            infecteds
                        INNER JOIN inventories ON
                            infecteds.survivor_id = inventories.survivor_id
                        WHERE
                            infecteds.infected = 0
                    ) as total")
                ->join('inventories', 'inventories.survivor_id', '=', 'infecteds.survivor_id')
                ->join('items', 'items.id', '=', 'inventories.item_id')
                ->where('infected', 0)
                ->groupBy('items.id')
                ->get();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}