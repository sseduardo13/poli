<?php

namespace App\Repositories\Survivor;

use App\Models\Survivor;
use App\Repositories\Survivor\Contracts\FindSurvivorByIdRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindSurvivorByIdEloquentRepository implements FindSurvivorByIdRepository
{
    /**
     * @var Model|Survivor
     */
    private Model|Survivor $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Survivor();
    }

    /**
     * @param int $id
     * @return Survivor|Exception|null
     */
    public function find(int $id): Survivor|Exception|null
    {
        try {
            return $this->eloquentModel->find($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}