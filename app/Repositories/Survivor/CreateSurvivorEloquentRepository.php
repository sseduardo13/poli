<?php

namespace App\Repositories\Survivor;

use App\Models\Survivor;
use App\Repositories\Survivor\Contracts\CreateSurvivorRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CreateSurvivorEloquentRepository implements CreateSurvivorRepository
{
    /**
     * @var Model|Survivor
     */
    private Model|Survivor $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Survivor();
    }

    /**
     * @param array $attributes
     * @return Survivor|Exception
     * @throws Exception
     */
    public function create(array $attributes): Survivor|Exception
    {
        try {
            return $this->eloquentModel->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}