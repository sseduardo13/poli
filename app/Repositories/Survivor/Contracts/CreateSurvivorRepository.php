<?php

namespace App\Repositories\Survivor\Contracts;

use App\Models\Survivor;
use Exception;

interface CreateSurvivorRepository
{    
    /**
     * @param array $attributes
     * @return Survivor|Exception
     * @throws Exception
     */
    public function create(array $attributes): Survivor|Exception;
}