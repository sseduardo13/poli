<?php

namespace App\Repositories\Survivor\Contracts;

use App\Models\Survivor;
use Exception;

interface FindSurvivorByIdRepository
{
    /**
     * @param int $id
     * @return Survivor|Exception|null
     */
    public function find(int $id): Survivor|Exception|null;
}