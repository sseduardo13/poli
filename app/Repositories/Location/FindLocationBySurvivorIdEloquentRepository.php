<?php

namespace App\Repositories\Location;

use App\Models\Location;
use App\Repositories\Location\Contracts\FindLocationBySurvivorIdRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindLocationBySurvivorIdEloquentRepository implements FindLocationBySurvivorIdRepository
{
    /**
     * @var Model|Location
     */
    private Model|Location $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Location();
    }

    /**
     * @param int $survivorId
     * @return Location|Exception|null
     */
    public function findBySurvivorId(int $survivorId): Location|Exception|null
    {
        try {
            return $this->eloquentModel
                ->where('survivor_id', $survivorId)
                ->first();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}