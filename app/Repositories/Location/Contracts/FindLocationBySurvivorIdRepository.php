<?php

namespace App\Repositories\Location\Contracts;

use App\Models\Location;
use Exception;

interface FindLocationBySurvivorIdRepository
{
    /**
     * @param int $survivorId
     * @return Location|Exception|null
     */
    public function findBySurvivorId(int $survivorId): Location|Exception|null;
}