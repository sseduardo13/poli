<?php

namespace App\Repositories\Location\Contracts;

use App\Models\Location;
use Exception;

interface UpdateLocationRepository
{
    /**
     * @param array $attributes
     * @param int $id
     * @return Location|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Location|Exception;
}