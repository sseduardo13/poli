<?php

namespace App\Repositories\Location;

use App\Models\Location;
use App\Repositories\Location\Contracts\CreateLocationRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CreateLocationEloquentRepository implements CreateLocationRepository
{
    /**
     * @var Model|Location
     */
    private Model|Location $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Location();
    }

    /**
     * @param array $attributes
     * @return Location|Exception
     * @throws Exception
     */
    public function create(array $attributes): Location|Exception
    {
        try {
            return $this->eloquentModel->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}