<?php

namespace App\Repositories\Location;

use App\Models\Location;
use App\Repositories\Location\Contracts\UpdateLocationRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UpdateLocationEloquentRepository implements UpdateLocationRepository
{
    /**
     * @var Model|Location
     */
    private Model|Location $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Location();
    }

    /**
     * @param array $attributes
     * @param int $id
     * @return Location|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Location|Exception
    {
        try {

            $model = $this->eloquentModel->findOrFail($id);
            $model->fill($attributes);
            $model->save();

            return $model;

        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}