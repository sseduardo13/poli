<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class SurvivorRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'age' => 'required|numeric',
            'gender' => 'required|string',
            'last_location.latitude' => 'required|string',        
            'last_location.longitude' => 'required|string',
            'items.*.item' => 'string',      
            'items.*.quantity' => 'numeric'      
        ];
    }
    
    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'success' => false,
                'data' => $validator->errors()
            ])
        );
    }
}
