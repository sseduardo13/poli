<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class TradeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'transaction.*.sender_id' => 'required|numeric',
            'transaction.*.items.*.item_id' => 'required|numeric',
            'transaction.*.items.*.quantity' => 'required|numeric',
            'transaction.*.receiver_id' => 'required|numeric',     
        ];
    }
    
    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'success' => false,
                'data' => $validator->errors()
            ])
        );
    }
}
