<?php

namespace App\Http\Controllers;

use App\Http\Resources\InfectedResponse;
use App\Services\Infected\Contracts\UpdateInfectedServiceContract;
use Illuminate\Http\Request;

class InfectedController extends Controller
{
    /** 
     * @var UpdateInfectedServiceContract
     */
    private UpdateInfectedServiceContract $updateInfectedService;
    
    public function __construct(UpdateInfectedServiceContract $updateInfectedService)
    {
        $this->updateInfectedService = $updateInfectedService;
    }

    public function updateInfected(Request $request)
    {
        try {
            return (new InfectedResponse($this->updateInfectedService->update($request->all(), $request->id)));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
}
