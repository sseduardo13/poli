<?php

namespace App\Http\Controllers;

use App\Http\Requests\SurvivorRequest;
use App\Http\Resources\SurvivorResponse;
use App\Services\Survivor\Contracts\SurvivorServiceContract;

class SurvivorController extends Controller
{
    /** 
     * @var SurvivorServiceContract 
     */
    private SurvivorServiceContract $survivorService;
    
    public function __construct(SurvivorServiceContract $survivorService)
    {
        $this->survivorService = $survivorService;
    }

    public function addSurvivor(SurvivorRequest $request)
    {
        try {
            return (new SurvivorResponse($this->survivorService->addSurvivor($request->all())));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
}
