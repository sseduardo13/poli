<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationRequest;
use App\Http\Resources\LocationResponse;
use App\Services\Location\Contracts\UpdateLocationServiceContract;

class LocationController extends Controller
{
    /** 
     * @var UpdateLocationServiceContract
     */
    private UpdateLocationServiceContract $updateLocationService;
    
    public function __construct(UpdateLocationServiceContract $updateLocationService)
    {
        $this->updateLocationService = $updateLocationService;
    }

    public function updateLocation(LocationRequest $request)
    {
        try {
            return (new LocationResponse($this->updateLocationService->update($request->all(), $request->id)));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
}
