<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use App\Http\Resources\DeletedItemResponse;
use App\Services\Item\Contracts\CreateItemServiceContract;
use App\Services\Item\Contracts\DeleteItemServiceContract;
use App\Services\Item\Contracts\FindItemServiceContract;
use App\Services\Item\Contracts\UpdateItemServiceContract;

class ItemController extends Controller
{
    /** 
     * @var CreateItemServiceContract
     */
    private CreateItemServiceContract $createItemService;

    /** 
     * @var FindItemServiceContract
     */
    private FindItemServiceContract $findItemService;

    /** 
     * @var UpdateItemServiceContract
     */
    private UpdateItemServiceContract $updateItemService;
    
    /** 
     * @var DeleteItemServiceContract
     */
    private DeleteItemServiceContract $deleteItemService;
    
    /**
     * @param CreateItemServiceContract $createItemService
     * @param FindItemServiceContract $findItemService
     * @param UpdateItemServiceContract $updateItemService
     * @param DeleteItemServiceContract $deleteItemService
     */
    public function __construct(
        CreateItemServiceContract $createItemService,
        FindItemServiceContract $findItemService,
        UpdateItemServiceContract $updateItemService,
        DeleteItemServiceContract $deleteItemService
    )
    {
        $this->createItemService = $createItemService;
        $this->findItemService = $findItemService;
        $this->updateItemService = $updateItemService;
        $this->deleteItemService = $deleteItemService;
    }

    /**
     * Display all resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function showItems()
    {
        try {
            return $this->findItemService->findAll();
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ItemRequest $request
     * @return \Illuminate\Http\Response
     */
    public function addItem(ItemRequest $request)
    {
        try {
            return $this->createItemService->create($request->all());
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ItemRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateItem(ItemRequest $request, $id)
    {
        try {
            return $this->updateItemService->update($request->all(), $request->id);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteItem($id)
    {
        try {
            return (new DeletedItemResponse($this->deleteItemService->delete($id)));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
}
