<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReportInfectedsResponse;
use App\Http\Resources\ReportNonInfectedsResponse;
use App\Services\Report\Contracts\InfectedSurvivorsServiceContract;
use App\Services\Report\Contracts\NonInfectedSurvivorsServiceContract;
use App\Services\Report\Contracts\PointsLostServiceContract;
use App\Services\Report\Contracts\ResourceBySurvivorServiceContract;

class ReportController extends Controller
{
    /** 
     * @var InfectedSurvivorsServiceContract
     */
    private InfectedSurvivorsServiceContract $infectedSurvivorsService;
    
    /** 
     * @var NonInfectedSurvivorsServiceContract
     */
    private NonInfectedSurvivorsServiceContract $nonInfectedSurvivorsService;
    
    /** 
     * @var ResourceBySurvivorServiceContract
     */
    private ResourceBySurvivorServiceContract $resourceBySurvivorService;
    
    /** 
     * @var PointsLostServiceContract
     */
    private PointsLostServiceContract $pointsLostService;
    
    /**
     * @param InfectedSurvivorsServiceContract $infectedSurvivorsService
     * @param NonInfectedSurvivorsServiceContract $nonInfectedSurvivorsService
     * @param PointsLostServiceContract $pointsLostService;
     * @param ResourceBySurvivorServiceContract $resourceBySurvivorService
     */
    public function __construct(
        InfectedSurvivorsServiceContract $infectedSurvivorsService,
        NonInfectedSurvivorsServiceContract $nonInfectedSurvivorsService,
        PointsLostServiceContract $pointsLostService,
        ResourceBySurvivorServiceContract $resourceBySurvivorService
    )
    {
        $this->infectedSurvivorsService = $infectedSurvivorsService;
        $this->nonInfectedSurvivorsService = $nonInfectedSurvivorsService;
        $this->pointsLostService = $pointsLostService;
        $this->resourceBySurvivorService = $resourceBySurvivorService;
    }

    public function infecteds()
    {
        try {
            return (new ReportInfectedsResponse($this->infectedSurvivorsService->findAll()));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }

    public function survivors()
    {
        try {
            return (new ReportNonInfectedsResponse($this->nonInfectedSurvivorsService->findAll()));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }

    public function resources()
    {
        try {
            return $this->resourceBySurvivorService->find();
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }

    public function pointsLost()
    {
        try {
            return $this->pointsLostService->find();
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
}
