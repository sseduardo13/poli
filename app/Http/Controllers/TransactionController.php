<?php

namespace App\Http\Controllers;

use App\Http\Requests\TradeRequest;
use App\Http\Resources\TradeResponse;
use App\Services\Trade\Contracts\TradeServiceContract;

class TransactionController extends Controller
{
    /** 
     * @var TradeServiceContract 
     */
    private TradeServiceContract $tradeService;
    
    public function __construct(TradeServiceContract $tradeService)
    {
        $this->tradeService = $tradeService;
    }

    public function makeTransaction(TradeRequest $request)
    {
        try {
            return (new TradeResponse($this->tradeService->makeTransaction($request->transactions)));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
}
