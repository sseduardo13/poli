<?php

use App\Http\Controllers\InfectedController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SurvivorController;
use App\Http\Controllers\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('survivors', [SurvivorController::class, 'addSurvivor']);

Route::put('survivors/{id}/location', [LocationController::class, 'updateLocation']);

Route::put('infecteds/{id}', [InfectedController::class, 'updateInfected']);

Route::post('trades', [TransactionController::class, 'makeTransaction']);

Route::controller(ItemController::class)->group(function () {
    Route::get('items', 'showItems');
    Route::post('items', 'addItem');
    Route::put('items/{id}', 'updateItem');
    Route::delete('items/{id}', 'deleteItem');
});

Route::controller(ReportController::class)->group(function () {
    Route::get('reports/infecteds', 'infecteds');
    Route::get('reports/survivors', 'survivors');
    Route::get('reports/resources', 'resources');
    Route::get('reports/points/lost', 'pointsLost');
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
