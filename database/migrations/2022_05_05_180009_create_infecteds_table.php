<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infecteds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('survivor_id')->constrained('survivors');
            $table->boolean('infected')->default(false);
            $table->integer('register')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infecteds');
    }
};
