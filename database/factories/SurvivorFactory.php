<?php

namespace Database\Factories;

use App\Models\Survivor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class SurvivorFactory extends Factory
{
    protected $model = Survivor::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $gender = ['male', 'female'];

        return [
            'name' => $this->faker->name(),
            'age' => $this->faker->numberBetween(1,120),
            'gender' => $this->faker->randomElement($gender)
        ];
    }
}