<?php

namespace Database\Factories;

use App\Models\Infected;
use App\Models\Survivor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class InfectedFactory extends Factory
{
    protected $model = Infected::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'survivor_id' => Survivor::factory()->create(),
            'infected' => $this->faker->numberBetween(0, 1),
            'register' => $this->faker->numberBetween(1, 3)
        ];
    }
}