<?php

namespace Database\Factories;

use App\Models\Inventory;
use App\Models\Item;
use App\Models\Survivor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class InventoryFactory extends Factory
{
    protected $model = Inventory::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'survivor_id' => Survivor::factory()->create(),
            'item_id' => Item::factory()->create(),
            'quantity' => $this->faker->randomDigitNotZero()
        ];
    }
}