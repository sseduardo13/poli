<?php

namespace Database\Factories;

use App\Models\Location;
use App\Models\Survivor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class LocationFactory extends Factory
{
    protected $model = Location::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'survivor_id' => Survivor::factory()->create(),
            'latitude' => $this->faker->randomFloat(4),
            'longitude' => $this->faker->randomFloat(4)
        ];
    }
}