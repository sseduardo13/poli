<?php

namespace Database\Seeders;

use App\Services\Item\Contracts\CreateItemServiceContract;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'water', 'points' => 4],
            ['name' => 'food', 'points' => 3],
            ['name' => 'medication', 'points' => 2],
            ['name' => 'ammunition', 'points' => 1]
        ];

        for ($i=0; $i < count($items); $i++) { 
            app(CreateItemServiceContract::class)->create($items[$i]);
        }
    }
}
