<?php

namespace Database\Seeders;

use App\Models\Infected;
use App\Models\Inventory;
use App\Models\Location;
use App\Models\Survivor;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SurvivorSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 2; $i++) { 
            $survivor = Survivor::factory()->create();
            
            Location::create([
                'survivor_id' => $survivor->id,
                'latitude' => '40.303' . $i,
                'longitude' => '5.321' . $i
            ]);
            
            Inventory::create([
                'survivor_id' => $survivor->id,
                'item_id' => $i+1,
                'quantity' => $i+1
            ]);
            
            Infected::create([
                'survivor_id' => $survivor->id,
                'infected' => 0,
                'register' => 0
            ]);
        }
    }
}
