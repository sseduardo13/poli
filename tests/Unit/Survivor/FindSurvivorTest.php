<?php

namespace Tests\Unit\Survivor;

use App\Models\Survivor;
use App\Services\Survivor\Contracts\FindSurvivorServiceContract;
use App\Services\Survivor\FindSurvivorService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FindSurvivorTest extends TestCase
{
    use RefreshDatabase;

    protected FindSurvivorServiceContract $findSurvivorService;
    protected Generator $faker;
    protected Survivor $survivor;

    public function setUp(): void
    {
        parent::setUp();

        $this->findSurvivorService = app(FindSurvivorService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindSurvivor()
    {
        $this->survivor = Survivor::factory()->create();

        $survivor = $this->findSurvivorService->find($this->survivor->id);

        $this->assertNotNull($survivor);
    }

    /**
     * @throws \Exception
     */
    public function testFindSurvivorNotFound()
    {
        Survivor::factory()->create();

        $survivor = $this->findSurvivorService->find(99);

        $this->assertNull($survivor);
    }
}