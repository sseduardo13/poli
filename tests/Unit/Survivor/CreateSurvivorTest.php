<?php

namespace Tests\Unit\Survivor;

use App\Services\Survivor\Contracts\CreateSurvivorServiceContract;
use App\Services\Survivor\CreateSurvivorService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateSurvivorTest extends TestCase
{
    use RefreshDatabase;

    protected CreateSurvivorServiceContract $createSurvivorService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->createSurvivorService = app(CreateSurvivorService::class);
        $this->faker = Factory::create();
    }

    public function testCreateSurvivorSuccess()
    {
        $nameFaker = $this->faker->name();
        $ageFaker = $this->faker->numberBetween(1,120);
        $genderFaker = 'Male';

        $survivor = $this->createSurvivorService->create([
            'name' => $nameFaker,
            'age' => $ageFaker,
            'gender' => $genderFaker
        ]);

        $this->assertEquals($nameFaker, $survivor->name);
        $this->assertEquals($ageFaker, $survivor->age);
        $this->assertEquals($genderFaker, $survivor->gender);
    }

    public function testCreateSurvivorFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->createSurvivorService->create([
            'name' => null,
            'age' => false,
            'gender' => 99
        ]);
    }
}