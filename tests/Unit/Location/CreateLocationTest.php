<?php

namespace Tests\Unit\Location;

use App\Models\Survivor;
use App\Services\Location\Contracts\CreateLocationServiceContract;
use App\Services\Location\CreateLocationService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateLocationTest extends TestCase
{
    use RefreshDatabase;

    protected CreateLocationServiceContract $createLocationService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->createLocationService = app(CreateLocationService::class);
        $this->faker = Factory::create();
    }

    public function testCreateLocationSuccess()
    {
        $survivorFaker = Survivor::factory()->create();
        $latitudeFaker = $this->faker->randomFloat(4);
        $longitudeFaker = $this->faker->randomFloat(4);

        $location = $this->createLocationService->create([
            'survivor_id' => $survivorFaker->id,
            'latitude' => $latitudeFaker,
            'longitude' => $longitudeFaker
        ]);

        $this->assertEquals($survivorFaker->id, $location->survivor_id);
        $this->assertEquals($latitudeFaker, $location->latitude);
        $this->assertEquals($longitudeFaker, $location->longitude);
    }

    public function testCreateLocationFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->createLocationService->create([
            'survivor_id' => "",
            'latitude' => null,
            'longitude' => false
        ]);
    }
}