<?php

namespace Tests\Unit\Location;

use App\Models\Location;
use App\Services\Location\Contracts\UpdateLocationServiceContract;
use App\Services\Location\UpdateLocationService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateLocationTest extends TestCase
{
    use RefreshDatabase;

    protected UpdateLocationServiceContract $updateLocationService;
    protected Generator $faker;
    protected Location $location;

    public function setUp(): void
    {
        parent::setUp();

        $this->updateLocationService = app(UpdateLocationService::class);
        $this->faker = Factory::create();
        $this->location = Location::factory()->create();
    }

    public function testUpdateLocationSuccess()
    {
        $latitudeFaker = $this->faker->randomFloat(4);
        $longitudeFaker = $this->faker->randomFloat(4);

        $location = $this->updateLocationService->update([
            'latitude' => $latitudeFaker,
            'longitude' => $longitudeFaker
        ], $this->location->survivor_id);

        $this->assertEquals($this->location->id, $location->id);
        $this->assertEquals($latitudeFaker, $location->latitude);
        $this->assertEquals($longitudeFaker, $location->longitude);
    }

    public function testUpdateLocationFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->updateLocationService->update([
            'survivor_id' => "",
            'latitude' => null,
            'longitude' => false
        ], $this->location->survivor_id);
    }
}