<?php

namespace Tests\Unit\Location;

use App\Models\Location;
use App\Models\Survivor;
use App\Services\Location\Contracts\FindLocationServiceContract;
use App\Services\Location\FindLocationService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FindLocationTest extends TestCase
{
    use RefreshDatabase;

    protected FindLocationServiceContract $findLocationService;
    protected Generator $faker;
    protected Location $location;
    protected Survivor $survivor;

    public function setUp(): void
    {
        parent::setUp();

        $this->findLocationService = app(FindLocationService::class);
        $this->faker = Factory::create();
        $this->survivor = Survivor::factory()->create();
    }

    /**
     * @throws \Exception
     */
    public function testFindLocation()
    {
        $location = Location::factory()->create([
            'survivor_id' => $this->survivor->id,
        ]);

        $location = $this->findLocationService->findBySurvivorId($location->survivor_id);

        $this->assertNotNull($location);
    }

    /**
     * @throws \Exception
     */
    public function testFindLocationNotFound()
    {
        Location::factory()->create();

        $location = $this->findLocationService->findBySurvivorId(99);

        $this->assertNull($location);
    }
}