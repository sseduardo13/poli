<?php

namespace Tests\Unit\Inventory;

use App\Models\Item;
use App\Models\Survivor;
use App\Services\Inventory\Contracts\CreateInventoryServiceContract;
use App\Services\Inventory\CreateInventoryService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateInventoryTest extends TestCase
{
    use RefreshDatabase;

    protected CreateInventoryServiceContract $createInventoryService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->createInventoryService = app(CreateInventoryService::class);
        $this->faker = Factory::create();
    }

    public function testCreateInventorySuccess()
    {
        $survivorFaker = Survivor::factory()->create();
        $itemFaker = Item::factory()->create();
        $quantityFaker = $this->faker->randomDigitNotZero();

        $inventory = $this->createInventoryService->create([
            'survivor_id' => $survivorFaker->id,
            'item_id' => $itemFaker->id,
            'quantity' => $quantityFaker
        ]);

        $this->assertEquals($survivorFaker->id, $inventory->survivor_id);
        $this->assertEquals($itemFaker->id, $inventory->item_id);
        $this->assertEquals($quantityFaker, $inventory->quantity);
    }

    public function testCreateInventoryFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->createInventoryService->create([
            'survivor_id' => null,
            'item_id' => null,
            'quantity' => true
        ]);
    }
}