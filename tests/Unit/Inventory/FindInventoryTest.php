<?php

namespace Tests\Unit\Inventory;

use App\Models\Inventory;
use App\Models\Item;
use App\Models\Survivor;
use App\Services\Inventory\Contracts\FindInventoryServiceContract;
use App\Services\Inventory\FindInventoryService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FindInventoryTest extends TestCase
{
    use RefreshDatabase;

    protected FindInventoryServiceContract $findInventoryService;
    protected Generator $faker;
    protected Inventory $inventory;

    public function setUp(): void
    {
        parent::setUp();

        $this->findInventoryService = app(FindInventoryService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindInventory()
    {
        $survivor = Survivor::factory()->create();
        $item = Item::factory()->create();

        $inventory = Inventory::factory()->create([
            'survivor_id' => $survivor->id,
            'item_id' => $item->id
        ]);

        $inventory = $this->findInventoryService->findBySurvivorId($inventory->survivor_id);

        $this->assertNotNull($inventory);
    }

    /**
     * @throws \Exception
     */
    public function testFindInventoryNotFound()
    {
        Inventory::factory()->create();

        $inventory = $this->findInventoryService->findBySurvivorId(99);

        $this->assertNull($inventory);
    }
}