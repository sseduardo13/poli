<?php

namespace Tests\Unit\Infected;

use App\Models\Survivor;
use App\Services\Infected\Contracts\CreateInfectedServiceContract;
use App\Services\Infected\CreateInfectedService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateInfectedTest extends TestCase
{
    use RefreshDatabase;

    protected CreateInfectedServiceContract $createInfectedService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->createInfectedService = app(CreateInfectedService::class);
        $this->faker = Factory::create();
    }

    public function testCreateInfectedSuccess()
    {
        $survivorFaker = Survivor::factory()->create();
        $infectedFaker = $this->faker->numberBetween(0, 1);
        $registerFaker = $this->faker->numberBetween(1, 3);

        $infected = $this->createInfectedService->create([
            'survivor_id' => $survivorFaker->id,
            'infected' => $infectedFaker,
            'register' => $registerFaker
        ]);

        $this->assertEquals($survivorFaker->id, $infected->survivor_id);
        $this->assertEquals($infectedFaker, $infected->infected);
        $this->assertEquals($registerFaker, $infected->register);
    }

    public function testCreateInfectedFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->createInfectedService->create([
            'survivor_id' => "",
            'infected' => null,
            'register' => false
        ]);
    }
}