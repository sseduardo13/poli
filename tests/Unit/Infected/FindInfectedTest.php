<?php

namespace Tests\Unit\Infected;

use App\Models\Infected;
use App\Models\Survivor;
use App\Services\Infected\Contracts\FindInfectedServiceContract;
use App\Services\Infected\FindInfectedService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FindInfectedTest extends TestCase
{
    use RefreshDatabase;

    protected FindInfectedServiceContract $findInfectedService;
    protected Generator $faker;
    protected Infected $infected;
    protected Survivor $survivor;

    public function setUp(): void
    {
        parent::setUp();

        $this->findInfectedService = app(FindInfectedService::class);
        $this->faker = Factory::create();
        $this->survivor = Survivor::factory()->create();
    }

    /**
     * @throws \Exception
     */
    public function testFindInfected()
    {
        $infected = Infected::factory()->create([
            'survivor_id' => $this->survivor->id,
        ]);

        $infected = $this->findInfectedService->findBySurvivorId($infected->survivor_id);

        $this->assertNotNull($infected);
    }

    /**
     * @throws \Exception
     */
    public function testFindInfectedNotFound()
    {
        Infected::factory()->create();

        $infected = $this->findInfectedService->findBySurvivorId(99);

        $this->assertNull($infected);
    }
}