<?php

namespace Tests\Unit\Infected;

use App\Models\Infected;
use App\Services\Infected\Contracts\UpdateInfectedServiceContract;
use App\Services\Infected\UpdateInfectedService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateInfectedTest extends TestCase
{
    use RefreshDatabase;

    protected UpdateInfectedServiceContract $updateInfectedService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->updateInfectedService = app(UpdateInfectedService::class);
        $this->faker = Factory::create();
    }

    public function testUpdateInfectedSuccess()
    {
        $infected = Infected::factory()->create([
            'infected' => false,
            'register' => 2
        ]);

        $updatedInfected = $this->updateInfectedService->update([
            'register' => 1
        ], $infected->survivor_id);

        $this->assertEquals($infected->id, $updatedInfected->id);
        $this->assertTrue($updatedInfected->infected);
    }

    public function testUpdateInfectedFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->infected = Infected::factory()->create();

        $this->updateInfectedService->update([
            'survivor_id' => "",
            'infected' => null,
            'register' => false
        ], $this->infected->survivor_id);
    }
}