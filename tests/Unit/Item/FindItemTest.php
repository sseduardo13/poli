<?php

namespace Tests\Unit\Item;

use App\Models\Item;
use App\Services\Item\Contracts\FindItemServiceContract;
use App\Services\Item\FindItemService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FindItemTest extends TestCase
{
    use RefreshDatabase;

    protected FindItemServiceContract $findItemService;
    protected Generator $faker;
    protected Item $item;

    public function setUp(): void
    {
        parent::setUp();

        $this->findItemService = app(FindItemService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindItem()
    {
        $this->item = Item::factory()->create();

        $item = $this->findItemService->find($this->item->id);

        $this->assertNotNull($item);
    }

    /**
     * @throws \Exception
     */
    public function testFindItemNotFound()
    {
        Item::factory()->create();

        $item = $this->findItemService->find(99);

        $this->assertNull($item);
    }
}