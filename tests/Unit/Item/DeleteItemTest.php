<?php

namespace Tests\Unit\Item;

use App\Models\Item;
use App\Services\Item\Contracts\DeleteItemServiceContract;
use App\Services\Item\DeleteItemService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteItemTest extends TestCase
{
    use RefreshDatabase;

    protected DeleteItemServiceContract $deleteItemService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->deleteItemService = app(DeleteItemService::class);
        $this->faker = Factory::create();
    }

    public function testDeleteItemSuccess()
    {
        $item = Item::factory()->create();

        $deleted = $this->deleteItemService->delete($item->id);

        $this->assertEquals(1, $deleted);
    }
}