<?php

namespace Tests\Unit\Item;

use App\Services\Item\Contracts\CreateItemServiceContract;
use App\Services\Item\CreateItemService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateItemTest extends TestCase
{
    use RefreshDatabase;

    protected CreateItemServiceContract $createItemService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->createItemService = app(CreateItemService::class);
        $this->faker = Factory::create();
    }

    public function testCreateItemSuccess()
    {
        $nameFaker = $this->faker->word();
        $pointsFaker = $this->faker->numberBetween(1,10);

        $item = $this->createItemService->create([
            'name' => $nameFaker,
            'points' => $pointsFaker
        ]);

        $this->assertEquals($nameFaker, $item->name);
        $this->assertEquals($pointsFaker, $item->points);
    }

    public function testCreateItemFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->createItemService->create([
            'name' => 6,
            'points' => null
        ]);
    }
}