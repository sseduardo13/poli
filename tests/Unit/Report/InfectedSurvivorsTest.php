<?php

namespace Tests\Unit\Report;

use App\Models\Infected;
use App\Models\Survivor;
use App\Repositories\Infected\Contracts\UpdateInfectedRepository;
use App\Services\Report\Contracts\InfectedSurvivorsServiceContract;
use App\Services\Report\InfectedSurvivorsService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InfectedSurvivorsTest extends TestCase
{
    use RefreshDatabase;

    protected InfectedSurvivorsServiceContract $infectedSurvivorsService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->infectedSurvivorsService = app(InfectedSurvivorsService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindAllInfectedSurvivors()
    {
        for ($i=0; $i < 4; $i++) { 
            $survivor = Survivor::factory()->create();
            
            Infected::factory()->create([
                'survivor_id' => $survivor->id, 
                'infected' => false, 
                'register' => 0
            ]);
        }
            
        $infected = Infected::first();

        app(UpdateInfectedRepository::class)->update(['infected' => true], $infected->id);

        $infectedRate = $this->infectedSurvivorsService->findAll();

        $this->assertEquals(25, $infectedRate);
    }

}