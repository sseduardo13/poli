<?php

namespace Tests\Unit\Report;

use App\Models\Infected;
use App\Models\Inventory;
use App\Models\Item;
use App\Models\Survivor;
use App\Repositories\Infected\Contracts\UpdateInfectedRepository;
use App\Services\Report\Contracts\PointsLostServiceContract;
use App\Services\Report\PointsLostService;
use Database\Seeders\ItemSeeder;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PointsLostTest extends TestCase
{
    use RefreshDatabase;

    protected PointsLostServiceContract $pointsLostService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->pointsLostService = app(PointsLostService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindPointsLost()
    {
        $this->seed(ItemSeeder::class);
        
        $item = Item::first();
        $survivor = Survivor::factory()->create();

        Inventory::factory()->create([
            'survivor_id' => $survivor->id,
            'item_id' => $item->id,
            'quantity' => 10
        ]);
        
        Infected::factory()->create([
            'survivor_id' => $survivor->id, 
            'infected' => true, 
            'register' => 5
        ]);

        $pointsLost = $this->pointsLostService->find();

        $this->assertEquals(40, $pointsLost->first()->total);
    }

}