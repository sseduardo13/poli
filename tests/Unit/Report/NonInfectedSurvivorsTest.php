<?php

namespace Tests\Unit\Report;

use App\Models\Infected;
use App\Models\Survivor;
use App\Repositories\Infected\Contracts\UpdateInfectedRepository;
use App\Services\Report\Contracts\NonInfectedSurvivorsServiceContract;
use App\Services\Report\NonInfectedSurvivorsService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NonInfectedSurvivorsTest extends TestCase
{
    use RefreshDatabase;

    protected NonInfectedSurvivorsServiceContract $nonInfectedSurvivorsService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->nonInfectedSurvivorsService = app(NonInfectedSurvivorsService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindAllNonInfectedSurvivors()
    {
        for ($i=0; $i < 4; $i++) { 
            $survivor = Survivor::factory()->create();
            
            Infected::factory()->create([
                'survivor_id' => $survivor->id, 
                'infected' => false, 
                'register' => 0
            ]);
        }
            
        $infected = Infected::first();

        app(UpdateInfectedRepository::class)->update(['infected' => true], $infected->id);

        $nonInfectedRate = $this->nonInfectedSurvivorsService->findAll();

        $this->assertEquals(75, $nonInfectedRate);
    }

}