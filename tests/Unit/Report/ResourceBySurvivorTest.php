<?php

namespace Tests\Unit\Report;

use App\Models\Infected;
use App\Models\Inventory;
use App\Models\Item;
use App\Models\Survivor;
use App\Repositories\Infected\Contracts\UpdateInfectedRepository;
use App\Services\Report\Contracts\ResourceBySurvivorServiceContract;
use App\Services\Report\ResourceBySurvivorService;
use Database\Seeders\ItemSeeder;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ResourceBySurvivorTest extends TestCase
{
    use RefreshDatabase;

    protected ResourceBySurvivorServiceContract $resourceBySurvivorService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->resourceBySurvivorService = app(ResourceBySurvivorService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindResourceBySurvivor()
    {
        $this->seed(ItemSeeder::class);
        
        $items = Item::all()->toArray();

        for ($i=1; $i <= 4; $i++) { 
            $survivor = Survivor::factory()->create();
            
            Inventory::factory()->create([
                'survivor_id' => $survivor->id,
                'item_id' => $items[$i-1]['id'],
                'quantity' => $i * 3
            ]);
            
            Infected::factory()->create([
                'survivor_id' => $survivor->id, 
                'infected' => false, 
                'register' => 0
            ]);
        }

        $infected = Infected::first();
        app(UpdateInfectedRepository::class)->update(['infected' => true], $infected->id);

        $resources = $this->resourceBySurvivorService->find()->toArray();

        $this->assertEquals(2, $resources[0]['total']);
        $this->assertEquals(3, $resources[1]['total']);
        $this->assertEquals(4, $resources[2]['total']);
    }

}